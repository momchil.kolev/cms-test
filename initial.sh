#!/bin/bash

read -p "Full config (1) or just CMS install (2): " config

if [ $config == 1 ]
then

    # Create new user
    read -p "New Username: " username
    adduser $username

    # Grant admin rights
    usermod -aG sudo $username

    # Switch to user
    su $username

    # Update system
    sudo apt update && apt upgrade -y

    # Install node
    curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -
    sudo apt-get install -y nodejs

    # Configure npm global
    mkdir ~/.npm-global
    npm config set prefix '~/.npm-global'
    export PATH=~/npm-global/bin:$PATH
    source ~/.profile

    # Install other packages
    sudo apt install -y zsh tmux

    # Configure zsh
    chsh -s $(which zsh)
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

    # Configure vim
    cat > ~/.vimrc << EOF
        set tabstop=4
        set shiftwidth=4
        set expandtab
        set number 
EOF
    # Install docker
    curl -fsSL https://get.docker.com -o get-docker.sh
    chmod u+x get-docker.sh
    sh get-docker.sh
    sudo usermod -aG docker kolev
    su - $USER
    rm get-docker.sh

    # Create and go to app directory
    if [ ! -d /usr/src/app ]
    then
        mkdir -p /usr/src/app
        cd /usr/src/app
    fi
fi

# CMS Logic
PS3='Which CMS would you like to install: '
options=("Wordpress" "Ghost" "Directus" "Strapi")
select opt in "${options[@]}"
do
    case $opt in
        "Wordpress")
            echo "Installing Wordpress"
            break
            ;;
        "Ghost")
            echo "Installing Ghost"
            break
            ;;
    "Directus")
        echo "Installing Directus"
        break
        ;;
    "Strapi")
        echo "Installing Strapi"
        bash strapi.sh
            break
            ;;
        *) echo "Invalid option";;
    esac
done
