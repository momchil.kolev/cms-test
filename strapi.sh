#!/bin/bash

cd /usr/src/app

strapi_running="$(docker container inspect -f '{{.State.Running}}' strapi)"

if [ strapi_running = false ]
then
    docker run -d -p 1337:1337 --name strapi -v `pwd`/strapi-cms-test:/srv/app strapi/strapi
    echo "Container started on port localhost:1337"
else
    echo "Strapi is already running on localhost:1337"
fi
