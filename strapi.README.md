# How to start strapi service

1. Follow README.md
2. `./strapi.sh`
3. Go to *localhost:1337/admin*
4. Go to Content-Types Builder
5. Under collection types click on *Create new collection type*
6. Add *Post* as Display name and click Continue
7. Click on Text
8. Type in *Title*
9. Click on *Add another field*
10. Click on *Rich Text*
11. Type in *Content*
12. Click Finish
13. Click Save (upper right hand side)
14. Under Collection Types (Above plugins) click on Posts
15. Click on *+Add New Post*
16. For title type in *This setup should be automated*
17. For content paste in the below text and click save
18. Now Click on *Roles and Permissions* on the left
19. Next to Posts click on Select All, select something in the right field and the click save

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dapibus ac erat eget maximus. Integer tempor tortor vitae accumsan condimentum. Proin pellentesque nulla et augue rutrum posuere. Nullam tincidunt mi ac tortor laoreet, sit amet volutpat dolor fringilla. Sed enim purus, auctor ac blandit id, dignissim non nulla. Mauris pretium mi vitae auctor rhoncus. Morbi eu nisi diam. Aenean venenatis nulla non turpis sollicitudin eleifend. Mauris posuere ex et consequat imperdiet.

Nunc cursus malesuada neque at molestie. Donec suscipit metus ultrices nulla egestas feugiat. Vestibulum purus tortor, vulputate sed tortor vitae, ultrices bibendum sem. Suspendisse in nulla ligula. Mauris sed massa sit amet felis ultricies dictum quis non nisi. Maecenas id sagittis mauris, a fringilla erat. Nullam libero quam, convallis ut elit sed, egestas scelerisque felis. Integer pulvinar ex vel ligula egestas, in luctus dolor dignissim. Donec neque erat, ultricies vel hendrerit ut, placerat vel erat. Proin vulputate in erat quis accumsan. Nullam luctus efficitur enim ut ultrices.

Fusce laoreet enim ut dolor pharetra maximus. Nunc eleifend odio neque, ac aliquam sem vulputate non. Proin lectus risus, congue venenatis ante vel, placerat lacinia sem. Suspendisse leo erat, tristique vitae ipsum ut, vehicula varius nisi. Fusce magna arcu, posuere sit amet erat ut, volutpat rutrum risus. Nam hendrerit non orci quis iaculis. Sed semper imperdiet nisl nec placerat. Sed ac tempor nibh, ut laoreet nisi. Aliquam sit amet eros eget risus venenatis egestas vitae id ipsum. Aenean a bibendum nunc. Etiam eu posuere justo, at condimentum augue. Phasellus dolor risus, luctus id nibh non, venenatis scelerisque justo. Mauris eu dui a eros semper maximus. Sed quam dolor, pretium quis dictum ut, ultrices et dui. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam consectetur, augue et imperdiet aliquet, sem leo porttitor nunc, a vestibulum sem magna eget ligula.

Vivamus velit massa, maximus eu tellus nec, porttitor tincidunt diam. Mauris viverra sem at risus sollicitudin vestibulum. Maecenas laoreet libero nec dui venenatis, sit amet consequat orci lacinia. Sed a felis et elit lacinia ullamcorper auctor a velit. Proin eget nulla felis. Sed mi erat, laoreet at mattis eget, faucibus ac dolor. Cras arcu metus, interdum at pulvinar ac, euismod nec ipsum. Nam accumsan mi elit, vel iaculis lectus malesuada et. Vestibulum ultrices magna a aliquet condimentum. Nullam aliquet, augue non laoreet consequat, metus purus euismod arcu, sit amet rutrum dolor lectus rutrum ligula.

Nulla facilisi. Duis ut sapien ac libero volutpat varius. Nullam varius tellus et metus aliquam, a blandit leo imperdiet. Proin hendrerit sem felis, ac viverra est laoreet eget. Aliquam sed diam sit amet leo feugiat auctor vel sed mi. Sed a mauris gravida, scelerisque purus at, gravida ligula. Sed ultricies tincidunt ante, sed eleifend urna dapibus a. Integer sit amet diam nisi. Nullam in tellus vitae nibh tincidunt mattis. Integer lobortis, sapien eget porttitor suscipit, ante erat volutpat quam, quis rhoncus diam turpis ac nibh. 
